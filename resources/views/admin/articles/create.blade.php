@extends('layouts.admin')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Articles</h1>
    </div>

    {!! Form::open(['url' => route('admin.articles.store'), 'method' => 'post', 'enctype'=>"multipart/form-data"]) !!}
    <div class="row g-3">
        <div class="col-10">
            <label for="title" class="form-label">Title</label>
            {!! Form::text('title', '', ['class' => 'form-control']) !!}

            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="col-10">
            <label for="title" class="form-label">Body</label>
            {!! Form::textarea('body', '', ['class' => 'form-control']) !!}

            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="col-10">
            <label for="status" class="form-label">Status</label>
            {!! Form::select('status', [0 => 'inactive', 1 => 'active'], ['class' => 'form-control']) !!}


            @error('status')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="col-10">
            <label for="category_id" class="form-label">Status</label>
            {!! Form::select('category_id', $categories, ['class' => 'form-control']) !!}


            @error('category_id')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="col-10">
            <label for="image" class="form-label">image</label>
            {!! Form::file('image', ['class' => 'form-control']) !!}


            @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>


        <button class=" btn btn-primary" type="submit">Create</button>
    </div>
    {!! Form::close() !!}
@stop