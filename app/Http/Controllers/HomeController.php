<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = Article::with('category')->active()->get();

        return view('home.index', compact('articles'));
    }

    public function readMore($id)
    {
        $article = Article::where('id', $id)->with(['category', 'user'])->first();

        return view('home.read_more', compact('article'));
    }
}
