@extends('layouts.main')
@section('content')
    <main class="container">


          <div class="row">
            <div class="col-md-8">
                <article class="blog-post">
                    <h2 class="blog-post-title">{{$article->title}}</h2>
                    <p class="blog-post-meta">{{$article->created_at }} by <a href="#">{{$article->user->name}}</a></p>

                    {{$article->body}}
                </article><!-- /.blog-post -->
            </div>

            <div class="col-md-4">
                <div class="p-4 mb-3 bg-light rounded">
                    <h4 class="fst-italic">About</h4>
                    <p class="mb-0">Saw you downtown singing the Blues. Watch you circle the drain. Why don't you let me
                        stop by? Heavy is the head that <em>wears the crown</em>. Yes, we make angels cry, raining down
                        on earth from up above.</p>
                </div>
            </div>
        </div><!-- /.row -->
    </main><!-- /.container -->

@stop