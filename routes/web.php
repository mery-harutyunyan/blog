<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\ArticleController as AdminArticleController;
use App\Http\Controllers\User\ArticleController as UserArticleController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;


Route::post('/login', [LoginController::class, 'login']);

Auth::routes(['verify' => true]);
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::middleware(['admin', 'auth'])->prefix('admin')->name('admin.')->group(function () {
    Route::resource('users', UserController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('articles', AdminArticleController::class);
});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::resource('articles', UserArticleController::class);
});


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/read-more/{id}', [App\Http\Controllers\HomeController::class, 'readMore'])->name('read-more');
