@extends('layouts.main')
@section('content')
    <main class="container">


        <div class="row mb-2">
            @if($articles)
                @foreach($articles as $article)
                    <div class="col-md-6">
                        <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                            <div class="col p-4 d-flex flex-column position-static">
                                <strong class="d-inline-block mb-2 text-primary">{{$article->category->name}}</strong>
                                <h3 class="mb-0">{{$article->title}}</h3>
                                <div class="mb-1 text-muted">{{$article->created_at}}</div>
                                <p class="card-text mb-auto">{{substr($article->body, 0, 200)}}</p>
                                <a href="{{route('read-more', $article->id)}}" class="stretched-link">Continue
                                    reading</a>
                            </div>
                            <div class="col-auto d-none d-lg-block">

                                @if($article->image)
                                    <img class="bd-placeholder-img" width="200" height="250"
                                         src="{{url('/uploads/'.$article->image)}}">
                                @else
                                    <svg class="bd-placeholder-img" width="200" height="250"
                                         xmlns="http://www.w3.org/2000/svg" role="img"
                                         aria-label="Placeholder: Thumbnail"
                                         preserveAspectRatio="xMidYMid slice" focusable="false"><title>
                                            Placeholder</title>
                                        <rect width="100%" height="100%" fill="#55595c"/>
                                        <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                                    </svg>
                                @endif

                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                no articles found
            @endif
        </div>
    </main>
@stop