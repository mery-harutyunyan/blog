@extends('layouts.admin')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{route('admin.articles.create')}}" class="btn btn-sm btn-outline-secondary">Add</a>
            </div>
        </div>
    </div>


    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Image</th>
                <th>Category</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>{{$article->id}}</td>
                    <td>{{$article->title}}</td>
                    <td>
                        @if($article->image)
                            <img width="60" src="{{url('/uploads/'.$article->image)}}">
                        @endif


                    </td>
                    <td>{{$article->category->name}}</td>
                    <td>{{$article->status ? 'Yes' : 'No'}}</td>
                    <td>
                        <a href="{{route('admin.articles.edit',$article->id )}}" class="btn btn-success">Edit</a>
                        {!! Form::open(['method' => 'DELETE',
                                'route' => ['admin.articles.destroy', $article->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
@stop