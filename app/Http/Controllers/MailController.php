<?php

namespace App\Http\Controllers;

use App\Models\User;

class MailController extends Controller
{
    public static function notifyAdmin($user, $article)
    {
        $admin = User::where('is_admin', 1)->first();

        $details = [
            'user' => $user,
            'admin' => $admin,
            'article' => $article

        ];

        \Mail::to('harutyunyan.mery@gmail.com')->send(new \App\Mail\NotifyAdmin($details));
    }

    public static function notifyUser($user, $article, $action = 'edited')
    {
        $admin = User::where('is_admin', 1)->first();

        $details = [
            'user' => $user,
            'admin' => $admin,
            'article' => $article,
            'action' => $action
        ];

        \Mail::to('harutyunyan.mery@gmail.com')->send(new \App\Mail\NotifyUser($details));
    }
}
