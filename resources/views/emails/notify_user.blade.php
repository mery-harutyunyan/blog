<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Dear {{ $details['user']->name }}, </h1>
<p>Admin {{ $details['admin']->name }} has just {{ $details['action'] }} your article</p>

<p>Customer : </p>
<p>Article :
    @if($details['action'] == 'edited')
        <a href="{{route('read-more', $details['article']->id)}}">{{ $details['article']->title }}</a>
    @else
        {{ $details['article']->title }}
    @endif
</p>

</body>
</html>