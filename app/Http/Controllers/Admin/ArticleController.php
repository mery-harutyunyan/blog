<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Http\Requests\ArticlesRequest;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->pluck('name', 'id');

        return view('admin.articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticlesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticlesRequest $request)
    {

        $article = new Article;
        $article->user_id = Auth::id();
        $article->title = $request->get('title');
        $article->body = $request->get('body');
        $article->category_id = $request->get('category_id');
        $article->status = (int)$request->get('status');
        if ($request->image) {
            $fileName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('uploads'), $fileName);
            $article->image = $fileName;
        }
        $article->save();

        return redirect()->route('admin.articles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::where('id', $id)->with('category')->first();
        $categories = Category::all()->pluck('name', 'id');

        return view('admin.articles.edit', compact('article', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticlesRequest $request
     * @param  int $id
     * @return void
     */
    public function update(ArticlesRequest $request, $id)
    {
        $article = Article::find($id);

        $article->title = $request->get('title');
        $article->body = $request->get('body');
        $article->category_id = $request->get('category_id');
        $article->status = (int)$request->get('status');
        if ($request->image) {
            $fileName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('uploads'), $fileName);
            $article->image = $fileName;
        }
        $article->save();

        if ($article->user_id != Auth::id()) {
            $user = User::where('id', $article->user_id)->first();
            MailController::notifyUser($user, $article, 'edited');
        }

        return redirect()->route('admin.articles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $articleCopy = clone $article;

        if (!$article) {
            return redirect()->route('admin.articles.index');
        }
        $article->delete();

        if ($articleCopy->user_id != Auth::id()) {
            $user = User::where('id', $articleCopy->user_id)->first();
            MailController::notifyUser($user, $articleCopy, 'deleted');
        }

        // redirect
        return redirect()->route('admin.articles.index');
    }
}
