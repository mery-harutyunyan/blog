@extends('layouts.admin')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Categories</h1>
    </div>

    {!! Form::open(['url' => route('admin.categories.update', $category->id), 'method' => 'put']) !!}
    <div class="row g-3">
        <div class="col-10">
            <label for="name" class="form-label">Name</label>
            {!! Form::text('name', $category->name, ['class' => 'form-control']) !!}

            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <button class=" btn btn-primary" type="submit">Update</button>
    </div>
    {!! Form::close() !!}
@stop